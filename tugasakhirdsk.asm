name "PRAKTIKUM DASAR SISTEM KOMPUTER-SELASA-PUKUL 10.30 WIB 
org 100h  ;  digunakan untuk memberitahukan assembler agar program pada saat dijalankan
          ;  ditaruh mulai pada offset ke 100h (256) byte.

    mov ah,09h ; nilai servis untuk mencetak karakter. 
    mov cx,54  ; banyaknya karakter yang akan dicetak.
    mov bl, 10011110b   ; warna atau atribut dari karakter.  
    int 10h
    mov ah, 02h         ; memulai program            
    mov DL,'P'         ; kode ASCII  
    int 21h             ; untuk mencetak karakter
    mov DL,'R'         ; kode ASCII  
    int 21h             ; untuk mencetak karakter
    mov DL,'A'         ; kode ASCII  
    int 21h             ; untuk mencetak karakter
    mov DL,'K'         ; kode ASCII  
    int 21h             ; untuk mencetak karakter
    mov DL,'T'        
    int 21h            
    mov DL,'I'
    int 21h
    mov DL,'K'
    int 21h
    mov DL,'U'
    int 21h
    mov DL,'M'
    int 21h
    mov DL,' '
    int 21h
    mov DL,'D'
    int 21h
    mov DL,'A'
    int 21h
    mov DL,'S'
    int 21h
    mov DL,'A'
    int 21h
    mov DL,'R'
    int 21h
    mov DL,' '
    int 21h
    mov DL,'S'
    int 21h
    mov DL,'I'
    int 21h
    mov DL,'S'
    int 21h
    mov DL,'T'
    int 21h
    mov DL,'E'
    int 21h
    mov DL,'M'
    int 21h
    mov DL,' '
    int 21h
    mov DL,'K'
    int 21h
    mov DL,'O'
    int 21h
    mov DL,'M'
    int 21h
    mov DL,'P'
    int 21h
    mov DL,'U'
    int 21h
    mov DL,'T'
    int 21h
    mov DL,'E'
    int 21h
    mov DL,'R'
    int 21h
    mov DL,'-'
    int 21h
    mov DL,'S'
    int 21h
    mov DL,'E'
    int 21h
    mov DL,'L'
    int 21h
    mov DL,'A'
    int 21h
    mov DL,'S'
    int 21h
    mov DL,'A'
    int 21h
    mov DL,','
    int 21h
    mov DL,'P'
    int 21h
    mov DL,'U'
    int 21h
    mov DL,'K'
    int 21h
    mov DL,'U'
    int 21h
    mov DL,'L'
    int 21h
    mov DL,' '
    int 21h
    mov DL,'1'
    int 21h
    mov DL,'0'
    int 21h
    mov DL,'.'
    int 21h
    mov DL,'3'
    int 21h
    mov DL,'0'
    int 21h
    mov DL,' '
    int 21h
    mov DL,'W'
    int 21h
    mov DL,'I'
    int 21h
    mov DL,'B' 
    int 21h         ; untuk cetak karakter
    int 20h         ; program selesai.
    
Ret                 ; untuk kembali dari suatu subrutin program.
     
